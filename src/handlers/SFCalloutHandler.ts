import { Rest, SObject } from 'ts-force';
import { Case } from '../generated';
import { AuthInfo, Connection, Org } from '@salesforce/core';




// an async self invoking function (main)
export async function generateData(){
    console.log('auth');
    const { accessToken, instanceUrl } = await auth("santiago.rojas@nomo.tech.dev"); // update username!
    //instanceUrl = instanceUrl+"/";

    const restInstance = new Rest({
        accessToken,
        instanceUrl
      });
    console.log(accessToken);
    console.log(instanceUrl);

    // properties can be set during construction
    const caseObj = new Case({
        subject: 'New Case Test'
    },restInstance);

    await caseObj.insert();
    console.log(caseObj); // will be set

    const caseQueried = await Case.retrieve(
      (f) => ({
        select: [...f.select('id', 'caseNumber')],
        where: [{ field: 'id', val:String(caseObj.id) }],
      }),
      { restInstance },
    );
    
    console.log(caseQueried);

    const data = (await restInstance.request.post<SObject>(
            '/services/apexrest/AccountClosure/updateCaseWithFinalStatus',
            { caseId: caseQueried[0].caseNumber, transactionType:'sent'}
        )).data;
    console.log(data);

    const caseHandled = await Case.retrieve(
      (f) => ({
        select: [...f.select('id', 'caseNumber','status')],
        where: [{ field: 'id', val:String(caseObj.id) }],
      }),
      { restInstance },
    );
    // cleanup records
    console.log(caseHandled);
  return {result:'Done'};
}

async function auth(username: string) {
    const authInfo = await AuthInfo.create(username );
    const connection = await Connection.create(authInfo );
    const org = await Org.create( connection );
    await org.refreshAuth();
    return org.getConnection();
}