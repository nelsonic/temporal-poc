import { generateData } from '../handlers/SFCalloutHandler';

export async function makeHTTPRequest(): Promise<string> {
  const data = await generateData();
  return data.result;
}