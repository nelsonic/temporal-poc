import express from "express";
import { WorkflowClient } from '@temporalio/client';
import { httpWorkflow } from './workflows';

const app = express();
const port = 8888; // default port to listen
const client = new WorkflowClient();
// define a route handler for the default home page
app.get( "/", ( req, res ) => {
    res.send( "Hello world!" );
} );

app.post( `/api/case/update`, async ( req: any, res ) => {
    try {
        const result = await client.execute(httpWorkflow, {
            taskQueue: 'activities-examples',
            workflowId: 'activities-examples',
        });
        console.log(result); // 'The answer is 42'
        return res.json( { result } );
    } catch ( err ) {
        // tslint:disable-next-line:no-console
        console.error(err);
        return res.json( { error: "couldn't start temporal services" } );
    }
} );

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );

