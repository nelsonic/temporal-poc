import { Rest, RestObject, QueryOpts, SObject, sField, SalesforceFieldType, SFLocation, SFieldProperties, FieldResolver, SOQLQueryParams, buildQuery, FieldProps, PicklistConst, CalendarDate } from "ts-force";
import "./";

export type CaseFields = Partial<FieldProps<Case>>;

/**
 * Generated class for Case
 */
export class Case extends RestObject {
    @sField({ apiName: 'Id', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.ID, salesforceLabel: 'Case ID', externalId: false })
    public readonly id?: string | null;
    @sField({ apiName: 'IsDeleted', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Deleted', externalId: false })
    public readonly isDeleted?: boolean | null;
    @sField({ apiName: 'MasterRecord', createable: false, updateable: false, required: false, reference: () => { return Case }, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Master Record ID', externalId: false })
    public masterRecord?: Case;
    @sField({ apiName: 'MasterRecordId', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Master Record ID', externalId: false })
    public readonly masterRecordId?: string | null;
    @sField({ apiName: 'CaseNumber', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Case Number', externalId: false })
    public readonly caseNumber?: string | null;
    @sField({ apiName: 'ContactId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Contact ID', externalId: false })
    public contactId?: string | null;
    @sField({ apiName: 'AccountId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Name', externalId: false })
    public accountId?: string | null;
    @sField({ apiName: 'AssetId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Asset ID', externalId: false })
    public assetId?: string | null;
    @sField({ apiName: 'ProductId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Product ID', externalId: false })
    public productId?: string | null;
    @sField({ apiName: 'EntitlementId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Entitlement ID', externalId: false })
    public entitlementId?: string | null;
    @sField({ apiName: 'SourceId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Source ID', externalId: false })
    public sourceId?: string | null;
    @sField({ apiName: 'BusinessHoursId', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Business Hours ID', externalId: false })
    public businessHoursId?: string | null;
    @sField({ apiName: 'Parent', createable: false, updateable: false, required: false, reference: () => { return Case }, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Parent Case ID', externalId: false })
    public parent?: Case;
    @sField({ apiName: 'ParentId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Parent Case ID', externalId: false })
    public parentId?: string | null;
    @sField({ apiName: 'SuppliedName', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Name', externalId: false })
    public suppliedName?: string | null;
    @sField({ apiName: 'SuppliedEmail', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.EMAIL, salesforceLabel: 'Email Address', externalId: false })
    public suppliedEmail?: string | null;
    @sField({ apiName: 'SuppliedPhone', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Phone', externalId: false })
    public suppliedPhone?: string | null;
    @sField({ apiName: 'SuppliedCompany', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Company', externalId: false })
    public suppliedCompany?: string | null;
    @sField({ apiName: 'Type', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Case Type', externalId: false })
    public type?: string | null;
    @sField({ apiName: 'RecordTypeId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Record Type ID', externalId: false })
    public recordTypeId?: string | null;
    @sField({ apiName: 'Status', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Status', externalId: false })
    public status?: string | null;
    @sField({ apiName: 'Reason', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Case Reason', externalId: false })
    public reason?: string | null;
    @sField({ apiName: 'Origin', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Case Origin', externalId: false })
    public origin?: string | null;
    @sField({ apiName: 'Language', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Language', externalId: false })
    public language?: string | null;
    @sField({ apiName: 'Subject', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Subject', externalId: false })
    public subject?: string | null;
    @sField({ apiName: 'Priority', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Priority', externalId: false })
    public priority?: string | null;
    @sField({ apiName: 'Description', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Description', externalId: false })
    public description?: string | null;
    @sField({ apiName: 'IsClosed', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Closed', externalId: false })
    public readonly isClosed?: boolean | null;
    @sField({ apiName: 'ClosedDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Closed Date', externalId: false })
    public readonly closedDate?: Date | null;
    @sField({ apiName: 'IsEscalated', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Escalated', externalId: false })
    public isEscalated?: boolean | null;
    @sField({ apiName: 'OwnerId', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Owner ID', externalId: false })
    public ownerId?: string | null;
    @sField({ apiName: 'SlaStartDate', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Entitlement Process Start Time', externalId: false })
    public slaStartDate?: Date | null;
    @sField({ apiName: 'SlaExitDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Entitlement Process End Time', externalId: false })
    public readonly slaExitDate?: Date | null;
    @sField({ apiName: 'IsStopped', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Stopped', externalId: false })
    public isStopped?: boolean | null;
    @sField({ apiName: 'StopStartDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Stopped Since', externalId: false })
    public readonly stopStartDate?: Date | null;
    @sField({ apiName: 'CreatedDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Created Date', externalId: false })
    public readonly createdDate?: Date | null;
    @sField({ apiName: 'CreatedById', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Created By ID', externalId: false })
    public readonly createdById?: string | null;
    @sField({ apiName: 'LastModifiedDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Last Modified Date', externalId: false })
    public readonly lastModifiedDate?: Date | null;
    @sField({ apiName: 'LastModifiedById', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Last Modified By ID', externalId: false })
    public readonly lastModifiedById?: string | null;
    @sField({ apiName: 'SystemModstamp', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'System Modstamp', externalId: false })
    public readonly systemModstamp?: Date | null;
    @sField({ apiName: 'ContactPhone', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PHONE, salesforceLabel: 'Contact Phone', externalId: false })
    public readonly contactPhone?: string | null;
    @sField({ apiName: 'ContactMobile', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PHONE, salesforceLabel: 'Contact Mobile', externalId: false })
    public readonly contactMobile?: string | null;
    @sField({ apiName: 'ContactEmail', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.EMAIL, salesforceLabel: 'Contact Email', externalId: false })
    public readonly contactEmail?: string | null;
    @sField({ apiName: 'ContactFax', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PHONE, salesforceLabel: 'Contact Fax', externalId: false })
    public readonly contactFax?: string | null;
    @sField({ apiName: 'Comments', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Internal Comments', externalId: false })
    public comments?: string | null;
    @sField({ apiName: 'LastViewedDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Last Viewed Date', externalId: false })
    public readonly lastViewedDate?: Date | null;
    @sField({ apiName: 'LastReferencedDate', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Last Referenced Date', externalId: false })
    public readonly lastReferencedDate?: Date | null;
    @sField({ apiName: 'ServiceContractId', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Service Contract ID', externalId: false })
    public serviceContractId?: string | null;
    @sField({ apiName: 'MilestoneStatus', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Milestone Status', externalId: false })
    public readonly milestoneStatus?: string | null;
    @sField({ apiName: 'First_Name__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'First Name', externalId: false })
    public firstName?: string | null;
    @sField({ apiName: 'Last_Name__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Last Name', externalId: false })
    public lastName?: string | null;
    @sField({ apiName: 'Date_of_Birth__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATE, salesforceLabel: 'Date of Birth', externalId: false })
    public dateOfBirth?: CalendarDate | null;
    @sField({ apiName: 'Country__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Country', externalId: false })
    public country?: string | null;
    @sField({ apiName: 'acknowledgement_sent__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Acknowledgement sent', externalId: false })
    public acknowledgementSent?: string | null;
    @sField({ apiName: 'Document_Type__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Document Type', externalId: false })
    public documentType?: string | null;
    @sField({ apiName: 'Document_Number__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'ID&V Gov. ID number', externalId: false })
    public documentNumber?: string | null;
    @sField({ apiName: 'delivery_address_line_1__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Delivery Address line 1', externalId: false })
    public deliveryAddressLine1?: string | null;
    @sField({ apiName: 'Date_of_Expiry__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATE, salesforceLabel: 'Expiry date of Gov. ID number', externalId: false })
    public dateOfExpiry?: CalendarDate | null;
    @sField({ apiName: 'MRZ_Line_1__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'MRZ Line 1', externalId: false })
    public mrzLine1?: string | null;
    @sField({ apiName: 'MRZ_Line_2__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'MRZ Line 2', externalId: false })
    public mrzLine2?: string | null;
    @sField({ apiName: 'Report_Link__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.URL, salesforceLabel: 'Onfido report', externalId: false })
    public reportLink?: string | null;
    @sField({ apiName: 'Result__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Case Verification Result', externalId: false })
    public result?: string | null;
    @sField({ apiName: 'final_response_sent__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Final response sent', externalId: false })
    public finalResponseSent?: string | null;
    @sField({ apiName: 'amazonconnect__AC_Contact_Id__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Amazon Connect Contact Id', externalId: true })
    public acContactId?: string | null;
    @sField({ apiName: 'Reference_Id__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Reference Id', externalId: true })
    public referenceId?: string | null;
    @sField({ apiName: 'Issuing_Country__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Issuing Country', externalId: false })
    public issuingCountry?: string | null;
    @sField({ apiName: 'Gender__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Gender', externalId: false })
    public gender?: string | null;
    @sField({ apiName: 'IDV_Status__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'ID&V Status', externalId: false })
    public idvStatus?: string | null;
    @sField({ apiName: 'Customer_Reference_Id__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Customer Reference Id', externalId: false })
    public readonly customerReferenceId?: string | null;
    @sField({ apiName: 'Nationality__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Nationality', externalId: false })
    public nationality?: string | null;
    @sField({ apiName: 'Case_Reason__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Case Reason', externalId: false })
    public caseReason?: string | null;
    @sField({ apiName: 'Customer_Status__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Customer Status', externalId: false })
    public customerStatus?: string | null;
    @sField({ apiName: 'Refinitiv_case_ID__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.URL, salesforceLabel: 'Refinitiv case', externalId: false })
    public refinitivCaseId?: string | null;
    @sField({ apiName: 'Screening_status__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Screening status', externalId: false })
    public screeningStatus?: string | null;
    @sField({ apiName: 'Sanction_check_passed__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Sanction check passed', externalId: false })
    public sanctionCheckPassed?: string | null;
    @sField({ apiName: 'Pep_check_passed__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Pep check passed', externalId: false })
    public pepCheckPassed?: string | null;
    @sField({ apiName: 'Adverse_media_check_passed__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Adverse media check passed', externalId: false })
    public adverseMediaCheckPassed?: string | null;
    @sField({ apiName: 'Address_verification_status__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Address verification status', externalId: false })
    public addressVerificationStatus?: string | null;
    @sField({ apiName: 'Address_line_1__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Address line 1', externalId: false })
    public addressLine1?: string | null;
    @sField({ apiName: 'Address_line_2__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Address line 2', externalId: false })
    public addressLine2?: string | null;
    @sField({ apiName: 'City__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'City', externalId: false })
    public city?: string | null;
    @sField({ apiName: 'Postal_code__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Postal code', externalId: false })
    public postalCode?: string | null;
    @sField({ apiName: 'Residence_country__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Residence country', externalId: false })
    public residenceCountry?: string | null;
    @sField({ apiName: 'Risk_Rating__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Overall Risk Rating', externalId: false })
    public riskRating?: string | null;
    @sField({ apiName: 'Country_risk_score__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Country risk score', externalId: false })
    public countryRiskScore?: number | null;
    @sField({ apiName: 'Customer_risk_score__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Customer risk score', externalId: false })
    public customerRiskScore?: number | null;
    @sField({ apiName: 'delivery_address_line_2__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Delivery Address line 2', externalId: false })
    public deliveryAddressLine2?: string | null;
    @sField({ apiName: 'delivery_postal_code__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Delivery Postal code', externalId: false })
    public deliveryPostalCode?: string | null;
    @sField({ apiName: 'delivery_city__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Delivery City', externalId: false })
    public deliveryCity?: string | null;
    @sField({ apiName: 'Under_review__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Under review', externalId: false })
    public underReview?: boolean | null;
    @sField({ apiName: 'financial_crime_memo__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Financial crime memo', externalId: false })
    public financialCrimeMemo?: string | null;
    @sField({ apiName: 'payment_date_and_time__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Payment date and time', externalId: false })
    public paymentDateAndTime?: Date | null;
    @sField({ apiName: 'amount_gbp__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Amount (GBP)', externalId: false })
    public amountGbp?: number | null;
    @sField({ apiName: 'foreign_amount__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Foreign amount', externalId: false })
    public foreignAmount?: number | null;
    @sField({ apiName: 'foreign_currency__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Foreign currency', externalId: false })
    public foreignCurrency?: string | null;
    @sField({ apiName: 'funds_origin_country__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Funds origin country', externalId: false })
    public fundsOriginCountry?: string | null;
    @sField({ apiName: 'payment_scheme__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Payment scheme', externalId: false })
    public paymentScheme?: string | null;
    @sField({ apiName: 'faster_payment_id_mt103__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Faster Payment ID/MT103', externalId: false })
    public fasterPaymentIdMt103?: string | null;
    @sField({ apiName: 'name_of_beneficiary__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Name of beneficiary', externalId: false })
    public nameOfBeneficiary?: string | null;
    @sField({ apiName: 'response_sent__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Response sent', externalId: false })
    public responseSent?: string | null;
    @sField({ apiName: 'name_of_the_actual_beneficiary__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Name of the actual beneficiary', externalId: false })
    public nameOfTheActualBeneficiary?: string | null;
    @sField({ apiName: 'error_explanation__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Error explanation', externalId: false })
    public errorExplanation?: string | null;
    /**
     * Name of the beneficiary they intended to pay
     */
    @sField({ apiName: 'name_of_intended_beneficiary_to_pay__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Name of intended beneficiary to pay', externalId: false })
    public nameOfIntendedBeneficiaryToPay?: string | null;
    @sField({ apiName: 'resolution_details__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Resolution details', externalId: false })
    public resolutionDetails?: string | null;
    @sField({ apiName: 'previous_risk_score__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Previous risk score', externalId: false })
    public previousRiskScore?: number | null;
    @sField({ apiName: 'current_risk_score__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Current risk score', externalId: false })
    public currentRiskScore?: number | null;
    @sField({ apiName: 'Asset_location_country_code__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Asset location country code', externalId: false })
    public assetLocationCountryCode?: string | null;
    @sField({ apiName: 'Bank_account_location_country_code__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Bank account location country code', externalId: false })
    public bankAccountLocationCountryCode?: string | null;
    @sField({ apiName: 'Tax_country_codes__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Tax ID country code', externalId: false })
    public taxCountryCodes?: string | null;
    @sField({ apiName: 'Cifas_check_for_UK_residents__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Cifas check for UK residents', externalId: false })
    public cifasCheckForUkResidents?: string | null;
    @sField({ apiName: 'customer_approved_transactions__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Customer approved transactions?', externalId: false })
    public customerApprovedTransactions?: string | null;
    @sField({ apiName: 'transaction_id__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Transaction ID', externalId: false })
    public transactionId?: string | null;
    @sField({ apiName: 'under_investigation__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Under Investigation', externalId: false })
    public underInvestigation?: string | null;
    /**
     * Customer confirms that the payee used has been deleted
     */
    @sField({ apiName: 'payee_used_has_been_deleted__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Payee used has been deleted?', externalId: false })
    public payeeUsedHasBeenDeleted?: string | null;
    /**
     * Customer confirms that they don’t recognize the amount and/or senders details
     */
    @sField({ apiName: 'amount_or_senders_not_recognized__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Amount or senders not recognized?', externalId: false })
    public amountOrSendersNotRecognized?: string | null;
    /**
     * Confirmation that customer checked with the beneficiary that no funds have credited their account
     */
    @sField({ apiName: 'no_funds_have_creditied_to_beneficiary__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'No funds have credited to beneficiary?', externalId: false })
    public noFundsHaveCreditiedToBeneficiary?: string | null;
    @sField({ apiName: 'complaint_status__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Complaint status', externalId: false })
    public complaintStatus?: string | null;
    @sField({ apiName: 'delivery_country_code__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Delivery Country code', externalId: false })
    public deliveryCountryCode?: string | null;
    @sField({ apiName: 'x5_working_days_passed__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: '5 working days passed', externalId: false })
    public x5WorkingDaysPassed?: string | null;
    @sField({ apiName: 'response_status__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Response status', externalId: false })
    public responseStatus?: string | null;
    @sField({ apiName: 'card_lost__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Card lost?', externalId: false })
    public cardLost?: string | null;
    @sField({ apiName: 'redress_paid__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Redress paid', externalId: false })
    public redressPaid?: string | null;
    @sField({ apiName: 'redress_amount__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Redress amount', externalId: false })
    public redressAmount?: number | null;
    @sField({ apiName: 'authorized_push_payment_fraud__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Authorized Push Payment Fraud', externalId: false })
    public authorizedPushPaymentFraud?: string | null;
    @sField({ apiName: 'card_stolen__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Card stolen?', externalId: false })
    public cardStolen?: string | null;
    @sField({ apiName: 'case_owner_name__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Case Owner Name', externalId: false })
    public readonly caseOwnerName?: string | null;
    @sField({ apiName: 'new_card_requested__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'New card requested?', externalId: false })
    public newCardRequested?: string | null;
    /**
     * Location of the last card transaction attempt
     */
    @sField({ apiName: 'location_of_last_card_transaction__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Location of last card transaction', externalId: false })
    public locationOfLastCardTransaction?: string | null;
    @sField({ apiName: 'last_card_transaction_type__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Last Card transaction type', externalId: false })
    public lastCardTransactionType?: string | null;
    @sField({ apiName: 'card_available__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Card available?', externalId: false })
    public cardAvailable?: string | null;
    @sField({ apiName: 'merchant_name__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Merchant Name', externalId: false })
    public merchantName?: string | null;
    @sField({ apiName: 'transaction_recognized__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Transaction recognized?', externalId: false })
    public transactionRecognized?: string | null;
    /**
     * Card available and customer agrees to cancel and issue a new one
     */
    @sField({ apiName: 'cancel_and_issue_a_new_card__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Cancel and issue a new card?', externalId: false })
    public cancelAndIssueANewCard?: string | null;
    @sField({ apiName: 'recognized_transaction_details__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Recognized transaction details', externalId: false })
    public recognizedTransactionDetails?: string | null;
    @sField({ apiName: 'risk_assessment_done__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Risk assessment done', externalId: false })
    public riskAssessmentDone?: string | null;
    @sField({ apiName: 'closing_case__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Closing Case', externalId: false })
    public closingCase?: boolean | null;
    @sField({ apiName: 'financial_crime_memo_updates__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Financial crime memo updates', externalId: false })
    public financialCrimeMemoUpdates?: string | null;
    @sField({ apiName: 'sub_category__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Sub-category', externalId: false })
    public subCategory?: string | null;
    @sField({ apiName: 'aws_close_case_failed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'aws close case failed', externalId: false })
    public awsCloseCaseFailed?: boolean | null;
    @sField({ apiName: 'sub_sub_category__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Sub-sub category', externalId: false })
    public subSubCategory?: string | null;
    @sField({ apiName: 'created_by_aws__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Created by AWS', externalId: false })
    public readonly createdByAws?: boolean | null;
    @sField({ apiName: 'customer_identity_verified__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Customer identity verified', externalId: false })
    public customerIdentityVerified?: string | null;
    @sField({ apiName: 'onfido_retry_reason__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Onfido Retry Reason', externalId: false })
    public onfidoRetryReason?: string | null;
    @sField({ apiName: 'assigned_to_queue__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Assigned to queue?', externalId: false })
    public readonly assignedToQueue?: string | null;
    @sField({ apiName: 'Case_Record_Approved__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Case Record Approved', externalId: false })
    public caseRecordApproved?: boolean | null;
    /**
     * Select the case type to 'Change Customer Details' and enter new email address.
     */
    @sField({ apiName: 'email__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.EMAIL, salesforceLabel: 'Email', externalId: false })
    public email?: string | null;
    /**
     * Select the case type to 'Change Customer Details' and enter new phone number.
     */
    @sField({ apiName: 'phone_number__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PHONE, salesforceLabel: 'Phone Number', externalId: false })
    public phoneNumber?: string | null;
    @sField({ apiName: 'Case_Record_Rejected__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Case Record Rejected', externalId: false })
    public caseRecordRejected?: boolean | null;
    @sField({ apiName: 'Current_Address__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Current Address', externalId: false })
    public currentAddress?: string | null;
    @sField({ apiName: 'Updated_Address__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Updated Address', externalId: false })
    public updatedAddress?: string | null;
    @sField({ apiName: 'completed_by__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Completed By', externalId: false })
    public completedById?: string | null;
    @sField({ apiName: 'current_last_name__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Current Last Name', externalId: false })
    public currentLastName?: string | null;
    @sField({ apiName: 'current_first_name__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Current First Name', externalId: false })
    public currentFirstName?: string | null;
    @sField({ apiName: 'Change_Name_Warning__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Change Name Warning', externalId: false })
    public readonly changeNameWarning?: string | null;
    @sField({ apiName: 'names_match__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Names Match', externalId: false })
    public namesMatch?: boolean | null;
    @sField({ apiName: 'change_reason__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Change Reason', externalId: false })
    public changeReason?: string | null;
    @sField({ apiName: 'case_record_type_name__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Case Record Type Name', externalId: false })
    public readonly caseRecordTypeName?: string | null;
    /**
     * The user that accepted this case when escalated.
     */
    @sField({ apiName: 'EscalationAcceptedBy__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Escalation Accepted By', externalId: false })
    public escalationAcceptedById?: string | null;
    /**
     * The user that accepted to execute the Peer Review process.
     */
    @sField({ apiName: 'PeerReviewExecutedBy__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Peer Review Executed By', externalId: false })
    public peerReviewExecutedById?: string | null;
    @sField({ apiName: 'Onfido_Score__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Onfido Score', externalId: false })
    public onfidoScore?: string | null;
    @sField({ apiName: 'CaseAcceptedBy__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Case Accepted By', externalId: false })
    public caseAcceptedById?: string | null;
    @sField({ apiName: 'CaseClosedBy__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Case Closed By', externalId: false })
    public caseClosedById?: string | null;
    /**
     * Informative read-only checkbox. This system checkbox is predefined as marked. When you click the button save, it will register your PR rejection.
     */
    @sField({ apiName: 'Reject_PR__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Reject Peer Review', externalId: false })
    public rejectPr?: boolean | null;
    /**
     * The rejected case will go back to the Working stage. Please leave your comments so the PR requester will know how to proceed.
     */
    @sField({ apiName: 'Reject_PR_Comments__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Rejection Comments', externalId: false })
    public rejectPrComments?: string | null;
    /**
     * Informative read-only checkbox. This system checkbox is predefined as marked. When you click the button save, it will register your PR acceptance.
     */
    @sField({ apiName: 'Approve_PR__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Approve Peer Review', externalId: false })
    public approvePr?: boolean | null;
    /**
     * The accepted case will go into the closed stage. Please leave your closure comments.
     */
    @sField({ apiName: 'Approval_PR_Comments__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Approval Comments', externalId: false })
    public approvalPrComments?: string | null;
    @sField({ apiName: 'Case_Owner_is_User__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Case Owner is User', externalId: false })
    public readonly caseOwnerIsUser?: boolean | null;
    @sField({ apiName: 'Current_User_is_Peer_Reviewer__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Current User is Peer Reviewer', externalId: false })
    public readonly currentUserIsPeerReviewer?: boolean | null;
    @sField({ apiName: 'Send_PR__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Send for PR', externalId: false })
    public sendPr?: boolean | null;
    /**
     * Please provide the peer reviewer with any information you might consider essential to streamline the process.
     */
    @sField({ apiName: 'Comments_for_Peer_Reviewer__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Comments for Peer Reviewer', externalId: false })
    public commentsForPeerReviewer?: string | null;
    @sField({ apiName: 'Peer_Review_Requested_By__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Peer Review Requested By', externalId: false })
    public peerReviewRequestedById?: string | null;
    @sField({ apiName: 'Peer_Review_Stage__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Peer Review Stage', externalId: false })
    public peerReviewStage?: string | null;
    @sField({ apiName: 'Taken_from_Peer_Review_Queue_By__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Taken from Peer Review Queue By', externalId: false })
    public takenFromPeerReviewQueueById?: string | null;
    @sField({ apiName: 'Peer_Review_Processes_Started__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Peer Review Processes Started', externalId: false })
    public readonly peerReviewProcessesStarted?: number | null;
    @sField({ apiName: 'Peer_Review_Acceptances__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Peer Review Acceptances', externalId: false })
    public readonly peerReviewAcceptances?: number | null;
    @sField({ apiName: 'Peer_Review_Rejections__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DOUBLE, salesforceLabel: 'Peer Review Rejections', externalId: false })
    public readonly peerReviewRejections?: number | null;
    /**
     * If true, it indicates the Peer Review has been approved. If false, it means there is a PR in process, the Peer Review was rejected, or there is no current Peer Review in operation for this case.
     */
    @sField({ apiName: 'IsPeerReviewApproved__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Peer Review Approved?', externalId: false })
    public isPeerReviewApproved?: boolean | null;
    @sField({ apiName: 'Send_ESC__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Send for Escalation', externalId: false })
    public sendEsc?: boolean | null;
    @sField({ apiName: 'Current_User_is_Owner__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Current User is Owner', externalId: false })
    public readonly currentUserIsOwner?: boolean | null;
    @sField({ apiName: 'Approval_Type__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Approval Type', externalId: false })
    public approvalType?: string | null;
    @sField({ apiName: 'Cust_Onfido_Score__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Onfido Score', externalId: false })
    public custOnfidoScore?: string | null;
    @sField({ apiName: 'Reason_For_Contact__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Reason For Contact', externalId: false })
    public reasonForContact?: string | null;
    /**
     * If true, it indicates the case was escalated. If false, it means the issue wasn't escalated or escalated but turned back to working status.
     */
    @sField({ apiName: 'IsEscalatedApproved__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Escalated Case?', externalId: false })
    public isEscalatedApproved?: boolean | null;
    @sField({ apiName: 'POA_Link__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.URL, salesforceLabel: 'POA Link', externalId: false })
    public poaLink?: string | null;
    @sField({ apiName: 'Escalation_Requested_By__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.REFERENCE, salesforceLabel: 'Escalation Requested By', externalId: false })
    public escalationRequestedById?: string | null;
    @sField({ apiName: 'Account_Voided__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Account Voided', externalId: false })
    public accountVoided?: boolean | null;
    @sField({ apiName: 'Cards_Inactivated__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Cards Inactivated', externalId: false })
    public cardsInactivated?: boolean | null;
    @sField({ apiName: 'Account_Deactivated__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Account Deactivated', externalId: false })
    public accountDeactivated?: boolean | null;
    @sField({ apiName: 'Opening_Time__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.DATETIME, salesforceLabel: 'Opening Time', externalId: false })
    public readonly openingTime?: Date | null;
    /**
     * Star Logo: VIP customers are high net worth individuals and possibly a Nomo's C-Level Executive.
     * Person Logo: Usual Nomo customers.
     */
    @sField({ apiName: 'Customer_Type__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Customer Type', externalId: false })
    public readonly customerType?: string | null;
    @sField({ apiName: 'Nomo_Employee__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Nomo Employee', externalId: false })
    public readonly nomoEmployee?: boolean | null;
    @sField({ apiName: 'CasePriorityIcons__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Case Priority', externalId: false })
    public readonly casePriorityIcons?: string | null;
    @sField({ apiName: 'Closure_requested_by__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Closure requested by', externalId: false })
    public closureRequestedBy?: string | null;
    @sField({ apiName: 'Closure_Requested_Reason__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'Closure Requested Reason', externalId: false })
    public closureRequestedReason?: string | null;
    /**
     * The new value for Integration Updates
     */
    @sField({ apiName: 'New_Value__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'New Value', externalId: false })
    public newValue?: string | null;
    /**
     * The old value when modified by an integration
     */
    @sField({ apiName: 'Old_Value__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Old Value', externalId: false })
    public oldValue?: string | null;
    @sField({ apiName: 'Watchlist_Needed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Watchlist Needed', externalId: false })
    public watchlistNeeded?: boolean | null;
    @sField({ apiName: 'Review_Notification_Sent__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Review Notification Sent', externalId: false })
    public reviewNotificationSent?: boolean | null;
    @sField({ apiName: 'Notification_On_Friday_MSG__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'Notification On Friday MSG', externalId: false })
    public readonly notificationOnFridayMsg?: string | null;
    @sField({ apiName: 'Contact_Reason_Summary__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Contact Reason Summary', externalId: false })
    public contactReasonSummary?: string | null;
    @sField({ apiName: 'Application_details_received__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.TEXTAREA, salesforceLabel: 'Application details received', externalId: false })
    public applicationDetailsReceived?: string | null;
    @sField({ apiName: 'Documents_received__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Documents received', externalId: false })
    public documentsReceived?: boolean | null;
    @sField({ apiName: 'Documents_validated__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Documents validated', externalId: false })
    public documentsValidated?: boolean | null;
    @sField({ apiName: 'Valuation_instructed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Valuation instructed', externalId: false })
    public valuationInstructed?: boolean | null;
    @sField({ apiName: 'Valuation_Received__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Valuation Received', externalId: false })
    public valuationReceived?: boolean | null;
    @sField({ apiName: 'Lending_decision_accept__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Lending decision - accept', externalId: false })
    public lendingDecisionAccept?: boolean | null;
    @sField({ apiName: 'Lending_decision_decline__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Lending decision - decline', externalId: false })
    public lendingDecisionDecline?: boolean | null;
    @sField({ apiName: 'X4_eye_check_on_lending_decision__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: '4 eye check on lending decision', externalId: false })
    public x4EyeCheckOnLendingDecision?: boolean | null;
    @sField({ apiName: 'Offer_letter_issued__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Offer letter issued', externalId: false })
    public offerLetterIssued?: boolean | null;
    @sField({ apiName: 'Offer_letter_signed_by_customer_received__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Offer letter signed by customer received', externalId: false })
    public offerLetterSignedByCustomerReceived?: boolean | null;
    @sField({ apiName: 'Solicitor_instructed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Solicitor instructed', externalId: false })
    public solicitorInstructed?: boolean | null;
    @sField({ apiName: 'Solicitor_work_completed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Solicitor work completed', externalId: false })
    public solicitorWorkCompleted?: boolean | null;
    @sField({ apiName: 'CP_Checklist_updated__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'CP Checklist updated', externalId: false })
    public cpChecklistUpdated?: boolean | null;
    @sField({ apiName: 'Completion_date_received__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Completion date received', externalId: false })
    public completionDateReceived?: boolean | null;
    @sField({ apiName: 'X4_eye_check_on_case__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: '4 eye check on case', externalId: false })
    public x4EyeCheckOnCase?: boolean | null;
    @sField({ apiName: 'Murabaha_trades_completed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Murabaha trades completed', externalId: false })
    public murabahaTradesCompleted?: boolean | null;
    @sField({ apiName: 'Funds_sent_to_Bank_Solicitor__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Funds sent to Bank Solicitor', externalId: false })
    public fundsSentToBankSolicitor?: boolean | null;
    @sField({ apiName: 'Final_case_details_completed__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Final case details completed', externalId: false })
    public finalCaseDetailsCompleted?: boolean | null;
    @sField({ apiName: 'Check_for_Compliance__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Check for Compliance', externalId: false })
    public checkForCompliance?: boolean | null;
    /**
     * To know the stage of each milestone in the Case Entitlement
     */
    @sField({ apiName: 'MilestoneStageCase__c', createable: true, updateable: true, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.PICKLIST, salesforceLabel: 'MilestoneStageCase', externalId: false })
    public milestoneStageCase?: string | null;
    @sField({ apiName: 'MilestonStageCaseFormula__c', createable: false, updateable: false, required: false, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.STRING, salesforceLabel: 'MilestonStageCaseFormula', externalId: false })
    public readonly milestonStageCaseFormula?: string | null;
    @sField({ apiName: 'Demo_SLA__c', createable: true, updateable: true, required: true, reference: undefined, childRelationship: false, salesforceType: SalesforceFieldType.BOOLEAN, salesforceLabel: 'Demo SLA (delete afterward)', externalId: false })
    public demoSla?: boolean | null;

    constructor(fields?: CaseFields, restInstance?: Rest) {
        super('Case', restInstance);
        this.id = void 0;
        this.isDeleted = void 0;
        this.masterRecord = void 0;
        this.masterRecordId = void 0;
        this.caseNumber = void 0;
        this.contactId = void 0;
        this.accountId = void 0;
        this.assetId = void 0;
        this.productId = void 0;
        this.entitlementId = void 0;
        this.sourceId = void 0;
        this.businessHoursId = void 0;
        this.parent = void 0;
        this.parentId = void 0;
        this.suppliedName = void 0;
        this.suppliedEmail = void 0;
        this.suppliedPhone = void 0;
        this.suppliedCompany = void 0;
        this.type = void 0;
        this.recordTypeId = void 0;
        this.status = void 0;
        this.reason = void 0;
        this.origin = void 0;
        this.language = void 0;
        this.subject = void 0;
        this.priority = void 0;
        this.description = void 0;
        this.isClosed = void 0;
        this.closedDate = void 0;
        this.isEscalated = void 0;
        this.ownerId = void 0;
        this.slaStartDate = void 0;
        this.slaExitDate = void 0;
        this.isStopped = void 0;
        this.stopStartDate = void 0;
        this.createdDate = void 0;
        this.createdById = void 0;
        this.lastModifiedDate = void 0;
        this.lastModifiedById = void 0;
        this.systemModstamp = void 0;
        this.contactPhone = void 0;
        this.contactMobile = void 0;
        this.contactEmail = void 0;
        this.contactFax = void 0;
        this.comments = void 0;
        this.lastViewedDate = void 0;
        this.lastReferencedDate = void 0;
        this.serviceContractId = void 0;
        this.milestoneStatus = void 0;
        this.firstName = void 0;
        this.lastName = void 0;
        this.dateOfBirth = void 0;
        this.country = void 0;
        this.acknowledgementSent = void 0;
        this.documentType = void 0;
        this.documentNumber = void 0;
        this.deliveryAddressLine1 = void 0;
        this.dateOfExpiry = void 0;
        this.mrzLine1 = void 0;
        this.mrzLine2 = void 0;
        this.reportLink = void 0;
        this.result = void 0;
        this.finalResponseSent = void 0;
        this.acContactId = void 0;
        this.referenceId = void 0;
        this.issuingCountry = void 0;
        this.gender = void 0;
        this.idvStatus = void 0;
        this.customerReferenceId = void 0;
        this.nationality = void 0;
        this.caseReason = void 0;
        this.customerStatus = void 0;
        this.refinitivCaseId = void 0;
        this.screeningStatus = void 0;
        this.sanctionCheckPassed = void 0;
        this.pepCheckPassed = void 0;
        this.adverseMediaCheckPassed = void 0;
        this.addressVerificationStatus = void 0;
        this.addressLine1 = void 0;
        this.addressLine2 = void 0;
        this.city = void 0;
        this.postalCode = void 0;
        this.residenceCountry = void 0;
        this.riskRating = void 0;
        this.countryRiskScore = void 0;
        this.customerRiskScore = void 0;
        this.deliveryAddressLine2 = void 0;
        this.deliveryPostalCode = void 0;
        this.deliveryCity = void 0;
        this.underReview = void 0;
        this.financialCrimeMemo = void 0;
        this.paymentDateAndTime = void 0;
        this.amountGbp = void 0;
        this.foreignAmount = void 0;
        this.foreignCurrency = void 0;
        this.fundsOriginCountry = void 0;
        this.paymentScheme = void 0;
        this.fasterPaymentIdMt103 = void 0;
        this.nameOfBeneficiary = void 0;
        this.responseSent = void 0;
        this.nameOfTheActualBeneficiary = void 0;
        this.errorExplanation = void 0;
        this.nameOfIntendedBeneficiaryToPay = void 0;
        this.resolutionDetails = void 0;
        this.previousRiskScore = void 0;
        this.currentRiskScore = void 0;
        this.assetLocationCountryCode = void 0;
        this.bankAccountLocationCountryCode = void 0;
        this.taxCountryCodes = void 0;
        this.cifasCheckForUkResidents = void 0;
        this.customerApprovedTransactions = void 0;
        this.transactionId = void 0;
        this.underInvestigation = void 0;
        this.payeeUsedHasBeenDeleted = void 0;
        this.amountOrSendersNotRecognized = void 0;
        this.noFundsHaveCreditiedToBeneficiary = void 0;
        this.complaintStatus = void 0;
        this.deliveryCountryCode = void 0;
        this.x5WorkingDaysPassed = void 0;
        this.responseStatus = void 0;
        this.cardLost = void 0;
        this.redressPaid = void 0;
        this.redressAmount = void 0;
        this.authorizedPushPaymentFraud = void 0;
        this.cardStolen = void 0;
        this.caseOwnerName = void 0;
        this.newCardRequested = void 0;
        this.locationOfLastCardTransaction = void 0;
        this.lastCardTransactionType = void 0;
        this.cardAvailable = void 0;
        this.merchantName = void 0;
        this.transactionRecognized = void 0;
        this.cancelAndIssueANewCard = void 0;
        this.recognizedTransactionDetails = void 0;
        this.riskAssessmentDone = void 0;
        this.closingCase = void 0;
        this.financialCrimeMemoUpdates = void 0;
        this.subCategory = void 0;
        this.awsCloseCaseFailed = void 0;
        this.subSubCategory = void 0;
        this.createdByAws = void 0;
        this.customerIdentityVerified = void 0;
        this.onfidoRetryReason = void 0;
        this.assignedToQueue = void 0;
        this.caseRecordApproved = void 0;
        this.email = void 0;
        this.phoneNumber = void 0;
        this.caseRecordRejected = void 0;
        this.currentAddress = void 0;
        this.updatedAddress = void 0;
        this.completedById = void 0;
        this.currentLastName = void 0;
        this.currentFirstName = void 0;
        this.changeNameWarning = void 0;
        this.namesMatch = void 0;
        this.changeReason = void 0;
        this.caseRecordTypeName = void 0;
        this.escalationAcceptedById = void 0;
        this.peerReviewExecutedById = void 0;
        this.onfidoScore = void 0;
        this.caseAcceptedById = void 0;
        this.caseClosedById = void 0;
        this.rejectPr = void 0;
        this.rejectPrComments = void 0;
        this.approvePr = void 0;
        this.approvalPrComments = void 0;
        this.caseOwnerIsUser = void 0;
        this.currentUserIsPeerReviewer = void 0;
        this.sendPr = void 0;
        this.commentsForPeerReviewer = void 0;
        this.peerReviewRequestedById = void 0;
        this.peerReviewStage = void 0;
        this.takenFromPeerReviewQueueById = void 0;
        this.peerReviewProcessesStarted = void 0;
        this.peerReviewAcceptances = void 0;
        this.peerReviewRejections = void 0;
        this.isPeerReviewApproved = void 0;
        this.sendEsc = void 0;
        this.currentUserIsOwner = void 0;
        this.approvalType = void 0;
        this.custOnfidoScore = void 0;
        this.reasonForContact = void 0;
        this.isEscalatedApproved = void 0;
        this.poaLink = void 0;
        this.escalationRequestedById = void 0;
        this.accountVoided = void 0;
        this.cardsInactivated = void 0;
        this.accountDeactivated = void 0;
        this.openingTime = void 0;
        this.customerType = void 0;
        this.nomoEmployee = void 0;
        this.casePriorityIcons = void 0;
        this.closureRequestedBy = void 0;
        this.closureRequestedReason = void 0;
        this.newValue = void 0;
        this.oldValue = void 0;
        this.watchlistNeeded = void 0;
        this.reviewNotificationSent = void 0;
        this.notificationOnFridayMsg = void 0;
        this.contactReasonSummary = void 0;
        this.applicationDetailsReceived = void 0;
        this.documentsReceived = void 0;
        this.documentsValidated = void 0;
        this.valuationInstructed = void 0;
        this.valuationReceived = void 0;
        this.lendingDecisionAccept = void 0;
        this.lendingDecisionDecline = void 0;
        this.x4EyeCheckOnLendingDecision = void 0;
        this.offerLetterIssued = void 0;
        this.offerLetterSignedByCustomerReceived = void 0;
        this.solicitorInstructed = void 0;
        this.solicitorWorkCompleted = void 0;
        this.cpChecklistUpdated = void 0;
        this.completionDateReceived = void 0;
        this.x4EyeCheckOnCase = void 0;
        this.murabahaTradesCompleted = void 0;
        this.fundsSentToBankSolicitor = void 0;
        this.finalCaseDetailsCompleted = void 0;
        this.checkForCompliance = void 0;
        this.milestoneStageCase = void 0;
        this.milestonStageCaseFormula = void 0;
        this.demoSla = void 0;
        this.__UUID = Case.__UUID;
        this.initObject(fields);
        return new Proxy(this, this.safeUpdateProxyHandler);
    }

    public static API_NAME: 'Case' = 'Case';
    public readonly _TYPE_: 'Case' = 'Case';
    public static __UUID = Symbol();
    private static _fields: { [P in keyof FieldProps<Case>]: SFieldProperties; };

    public static get FIELDS() {
        return this._fields = this._fields ? this._fields : Case.getPropertiesMeta<FieldProps<Case>, Case>(Case)
    }

    public static async retrieve(qryParam: ((fields: FieldResolver<Case>) => SOQLQueryParams) | string, opts?: QueryOpts): Promise<Case[]> {

        const qry = typeof qryParam === 'function' ? buildQuery(Case, qryParam) : qryParam;
        return await RestObject.query<Case>(Case, qry, opts);

    }

    public static fromSFObject(sob: SObject): Case {
        return new Case().mapFromQuery(sob);
    }
}
